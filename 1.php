<?php
//Basic syntax
echo 'this is a test for php';
//variables
//Basics
$foo = 'Bob';
echo ($foo."\n");
$bar = &$foo;
$bar = "My name is $bar";
echo ($bar."\n");
var_dump($bar);
//Variable scope
$a = 1;
$b = 2;

function test1()
{

    $b = 5;
}
test1();
echo ($b."\n");

function Sum1()
{
    global $a, $b;

    $b = $a + $b;
} 

Sum1();
echo ($b."\n");

function Sum2()
{
    $GLOBALS['b'] = $GLOBALS['a'] + $GLOBALS['b'];
} 
Sum2();
echo ($b."\n");

//static variables

function test()
{
    static $a = 0;
    echo ($a."\n");
    $a++;
}
test();
test();
test();
//Variable variables
$a = 'hello';
$$a = 'world';
echo ("$a ${$a}"."\n");
echo ("$a $hello"."\n");
//types
//boolean
$a = True;
$b = False;
var_dump($b);
//Integers
$a = 1234;
var_dump($a);
//Floating point numbers 
$a = 1.234; 
$b = 1.2e3; 
$c = 7E-10;
//Strings
$a_1 = 'this is a simple string $b';
$a_2 = "this is a simple string $b";
var_dump($a_1);
echo ($a_1."\n");
echo ($a_2."\n");
